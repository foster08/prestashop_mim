<?php /* Smarty version Smarty-3.1.19, created on 2016-03-18 15:53:27
         compiled from "C:\wamp\www\prestashopv2\themes\default-bootstrap\advancedsearch.tpl" */ ?>
<?php /*%%SmartyHeaderCode:549156ec16671ffe60-71254613%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '52626f1612b3d27294ec48eaece7dcb1a819e231' => 
    array (
      0 => 'C:\\wamp\\www\\prestashopv2\\themes\\default-bootstrap\\advancedsearch.tpl',
      1 => 1458312636,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '549156ec16671ffe60-71254613',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'tailles' => 0,
    'taille' => 0,
    'colors' => 0,
    'color' => 0,
    'products' => 0,
    'product' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_56ec1667470ef9_30405520',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56ec1667470ef9_30405520')) {function content_56ec1667470ef9_30405520($_smarty_tpl) {?><form id="formulaire" method="post" action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('advancedsearch');?>
">
	<p class="form-group" id="search">
		<label for="recherche"><?php echo smartyTranslate(array('s'=>"Votre recherche"),$_smarty_tpl);?>
</label>
		<input type="text" id="search_query_top" name="search_query" class="form-control">
	</p>
	<p class="form-group taille">
		<label for="minimum"><?php echo smartyTranslate(array('s'=>"Prix minimum"),$_smarty_tpl);?>
</label>
		<input type="text" id="minimum" name="minimum" class="form-control">
	</p>
	<p class="form-group taille">
		<label for="maximum"><?php echo smartyTranslate(array('s'=>"Prix maximum"),$_smarty_tpl);?>
</label>
		<input type="text" id="maximum" name="maximum" class="form-control">
	</p>
	<p class="form-group">
		<label for="taille"><?php echo smartyTranslate(array('s'=>"Taille"),$_smarty_tpl);?>
</label>
		<select name="taille" id="taille" class="form-control">
			<?php  $_smarty_tpl->tpl_vars['taille'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['taille']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tailles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['taille']->key => $_smarty_tpl->tpl_vars['taille']->value) {
$_smarty_tpl->tpl_vars['taille']->_loop = true;
?>
			<option value="<?php echo $_smarty_tpl->tpl_vars['taille']->value['id_attribute'];?>
"><?php echo $_smarty_tpl->tpl_vars['taille']->value['name'];?>
</option>
			<?php } ?>
		</select>
	</p>
	<p class="form-group">
		<label for="color"><?php echo smartyTranslate(array('s'=>"Couleur"),$_smarty_tpl);?>
</label>
		<select name="color" id="color" style="width: 90px;" class="form-control">
			<?php  $_smarty_tpl->tpl_vars['color'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['color']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['colors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['color']->key => $_smarty_tpl->tpl_vars['color']->value) {
$_smarty_tpl->tpl_vars['color']->_loop = true;
?>
			<option value="<?php echo $_smarty_tpl->tpl_vars['color']->value['id_attribute'];?>
" style="background: <?php echo $_smarty_tpl->tpl_vars['color']->value['color'];?>
"></option>
			<?php } ?>
		</select>
	</p>
	<p class="form-group">
		<label for="dispo"><?php echo smartyTranslate(array('s'=>"Disponibilité"),$_smarty_tpl);?>
</label>
		<select name="dispo" id="dispo" class="form-control">
			<option value="in-stock"><?php echo smartyTranslate(array('s'=>"En stock"),$_smarty_tpl);?>
</option>
			<option value="out-of-stock"><?php echo smartyTranslate(array('s'=>"Pas disponible"),$_smarty_tpl);?>
</option>
		</select>
	</p>
	<p class="form-group submit">
		<button type="submit">
			<span>
				<span><?php echo smartyTranslate(array('s'=>"Rechercher"),$_smarty_tpl);?>
</span>
			</span>
		</button>
	</p>
</form>
<div class="content">
<?php if (isset($_smarty_tpl->tpl_vars['products']->value)) {?>
<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
	<?php echo $_smarty_tpl->tpl_vars['product']->value['name'];?>

<?php } ?>
<?php }?>

	<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./product-list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div><?php }} ?>
