<?php /* Smarty version Smarty-3.1.19, created on 2016-03-18 15:53:18
         compiled from "C:\wamp\www\prestashopv2\themes\default-bootstrap\modules\blocksearch\blocksearch-top.tpl" */ ?>
<?php /*%%SmartyHeaderCode:912656ec165e4d0951-83081126%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ccf77533463d4b8b1f7965be2e1324cf4c9b32a8' => 
    array (
      0 => 'C:\\wamp\\www\\prestashopv2\\themes\\default-bootstrap\\modules\\blocksearch\\blocksearch-top.tpl',
      1 => 1456394118,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '912656ec165e4d0951-83081126',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'search_query' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_56ec165e66eab7_71010562',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56ec165e66eab7_71010562')) {function content_56ec165e66eab7_71010562($_smarty_tpl) {?>
<!-- Block search module TOP -->
<div id="search_block_top" class="col-sm-4 clearfix">
	<form id="searchbox" method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('search',null,null,null,false,null,true), ENT_QUOTES, 'UTF-8', true);?>
" >
		<input type="hidden" name="controller" value="search" />
		<input type="hidden" name="orderby" value="position" />
		<input type="hidden" name="orderway" value="desc" />
		<input class="search_query form-control" type="text" id="search_query_top" name="search_query" placeholder="<?php echo smartyTranslate(array('s'=>'Search','mod'=>'blocksearch'),$_smarty_tpl);?>
" value="<?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['search_query']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
" />
		<button type="submit" name="submit_search" class="btn btn-default button-search">
			<span><?php echo smartyTranslate(array('s'=>'Search','mod'=>'blocksearch'),$_smarty_tpl);?>
</span>
		</button>
		<a class="btn btn-default button-advanced-search" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('advancedsearch');?>
"><span><?php echo smartyTranslate(array('s'=>"..."),$_smarty_tpl);?>
</span></a>
	</form>
</div>
<!-- /Block search module TOP --><?php }} ?>
