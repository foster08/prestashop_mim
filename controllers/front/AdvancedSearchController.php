<?php 


class AdvancedSearchControllerCore extends FrontController
{
	public $php_self = 'advancedsearch';

	public function initContent()
	{
		parent::initContent();

		$db = Db::getInstance();

		$req_taille = 'SELECT '._DB_PREFIX_.'attribute_lang.name, '._DB_PREFIX_.'attribute_lang.id_attribute  FROM '._DB_PREFIX_.'attribute_lang 
		INNER JOIN '._DB_PREFIX_.'attribute 
		ON '._DB_PREFIX_.'attribute_lang.id_attribute = '._DB_PREFIX_.'attribute.id_attribute 
		INNER JOIN '._DB_PREFIX_.'attribute_group_lang 
		ON  '._DB_PREFIX_.'attribute.id_attribute_group = '._DB_PREFIX_.'attribute_group_lang.id_attribute_group 
		WHERE '._DB_PREFIX_.'attribute_group_lang.id_attribute_group = 1';
		$tailles = Db::getInstance()->ExecuteS($req_taille);

		$req_colors = 'SELECT '._DB_PREFIX_.'attribute.color, '._DB_PREFIX_.'attribute.id_attribute FROM '._DB_PREFIX_.'attribute
		WHERE '._DB_PREFIX_.'attribute.id_attribute_group = 3';
		$colors = Db::getInstance()->ExecuteS($req_colors);
		if ($_POST) {
			$recherche = $_POST['search_query'];
			$min = $_POST['minimum'];
			$max = $_POST['maximum'];
			$taille = $_POST['taille'];
			$color = $_POST['color'];
			$dispo = $_POST['dispo'];
			$req_recherche = 'SELECT * FROM '._DB_PREFIX_.'product_lang WHERE name LIKE "%'.$recherche.'%"';
			$result = Db::getInstance()->ExecuteS($req_recherche);
		}
		if ($_POST) {
			$this->context->smarty->assign(array(
				'tailles' => $tailles,
				'colors' => $colors,
				'products' => $result
				));	
		}
		else {
			$this->context->smarty->assign(array(
				'tailles' => $tailles,
				'colors' => $colors
				));
		}

		$this->setTemplate(_PS_THEME_DIR_.'advancedsearch.tpl');
	}

}