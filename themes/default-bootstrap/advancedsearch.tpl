<form id="formulaire" method="post" action="{$link->getPageLink('advancedsearch')}">
	<p class="form-group" id="search">
		<label for="recherche">{l s="Votre recherche"}</label>
		<input type="text" id="search_query_top" name="search_query" class="form-control">
	</p>
	<p class="form-group taille">
		<label for="minimum">{l s="Prix minimum"}</label>
		<input type="text" id="minimum" name="minimum" class="form-control">
	</p>
	<p class="form-group taille">
		<label for="maximum">{l s="Prix maximum"}</label>
		<input type="text" id="maximum" name="maximum" class="form-control">
	</p>
	<p class="form-group">
		<label for="taille">{l s="Taille"}</label>
		<select name="taille" id="taille" class="form-control">
			{foreach from=$tailles item=taille}
			<option value="{$taille.id_attribute}">{$taille.name}</option>
			{/foreach}
		</select>
	</p>
	<p class="form-group">
		<label for="color">{l s="Couleur"}</label>
		<select name="color" id="color" style="width: 90px;" class="form-control">
			{foreach from=$colors item=color}
			<option value="{$color.id_attribute}" style="background: {$color.color}"></option>
			{/foreach}
		</select>
	</p>
	<p class="form-group">
		<label for="dispo">{l s="Disponibilité"}</label>
		<select name="dispo" id="dispo" class="form-control">
			<option value="in-stock">{l s="En stock"}</option>
			<option value="out-of-stock">{l s="Pas disponible"}</option>
		</select>
	</p>
	<p class="form-group submit">
		<button type="submit">
			<span>
				<span>{l s="Rechercher"}</span>
			</span>
		</button>
	</p>
</form>
<div class="content">
{if isset($products) }
{foreach from=$products item=product}
	{$product.name}
{/foreach}
{/if}

	{include file="$tpl_dir./product-list.tpl"}
</div>